﻿## Описание задания

Задача № 2: "Война в долине теней".

Ваш отряд встречает ужасного тролля, у которого здоровье равно сумме здоровья всех ваших бойцов, сила удара и защита равны силе удара и защите самого сильного и защищённого из бойцов, соответственно, а все остальные характеристики равны минимальной из всего вашего отряда. Как победить тролля с минимальными потерями?
В битве урон здоровью после удара вычисляется по формуле:
(1) Урон1 = |Защита1 + Уклонение1 – Сила_удара2 – Ловкость2|

## Решение
Для решения задачи были созданы два новых класса Troll и Fight.

```cpp
class Troll:public ICollectable
{
public:


	Troll() = delete;
	Troll(int strength, int defence, int health, int dexterity, int dodging) {
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
	}

	void print() override;

	virtual bool write(std::ostream& os) override {
		return true;
	};

	bool isDead() override{
		return health <= 0;
	}

	~Troll() {};

};

class Fight {
public:
	vector<std::shared_ptr<ICollectable>> _squads;
	shared_ptr<ICollectable> _troll;

	Fight(vector<std::shared_ptr<ICollectable>> squads);

	void squadManagment();

	int strike(shared_ptr<ICollectable> hitter, shared_ptr<ICollectable> taker);

	void doFight();

	bool endOfFight() {

		return _troll->isDead()||_squads.size()==0;
	}

	void print();
};
```

## Выводы

В результате выполнения лабораторной работы была произведена одна итерация. Произведен рефакторинг, старые тесты всё также выполняются, были написаны новые тесты, которые также проходят.

