#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"


using namespace std;

class Squad:public ICollectable {
public:
	enum Fraction {
		MAGISTER,
		PALADIN,
		GODWOKEN

	};
	enum FightType {
		MELEE,
		RANGED
	};
	Fraction fraction;
	FightType fightType;

	bool invariant();
	
	Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft);

	Squad(int f, int strength, int defence, int health, int dexterity, int dodging, int ft);
	Squad() {

	};

	virtual bool write(std::ostream& os) override;

	void print() override;
	bool isDead() override{
		return health <= 0;
	}

    ~Squad() {};
};

class ItemCollector: public ACollector
{
public:
	vector<shared_ptr<ICollectable>> getCollection();
    virtual shared_ptr<ICollectable> read(istream& is, bool & flag) override;
};

class Troll:public ICollectable
{
public:


	Troll() = delete;
	Troll(int strength, int defence, int health, int dexterity, int dodging) {
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
	}

	void print() override;

	virtual bool write(std::ostream& os) override {
		return true;
	};

	bool isDead() override{
		return health <= 0;
	}

	~Troll() {};

};

class Fight {
public:
	vector<std::shared_ptr<ICollectable>> _squads;
	shared_ptr<ICollectable> _troll;

	Fight(vector<std::shared_ptr<ICollectable>> squads);

	void squadManagment();

	int strike(shared_ptr<ICollectable> hitter, shared_ptr<ICollectable> taker);

	void doFight();

	bool endOfFight() {

		return _troll->isDead()||_squads.size()==0;
	}

	void print();
};

#endif // HW_L3_DOMAIN_LAYER_H
