#ifndef HW_L4_INFRASTRUCTURE_LAYER_H
#define HW_L4_INFRASTRUCTURE_LAYER_H

#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <memory>
#include <sstream>
#include <fstream>

template<typename T>
T readItem(std::istream& is, bool & flag)
{
	T result;
	if (is.read((char *)(&result), sizeof(result))) {
		flag = !is.eof();
		return result;
	}
	else {
		flag = false;
		return result;
	}
}

class ICollectable
{
public:
	int strength;
	int defence;
	int health;
	int dexterity;
	int dodging;
	virtual ~ICollectable() = default;
	virtual void print() = 0;
	virtual bool write(std::ostream& os) = 0;
	virtual bool isDead() = 0;
};


class ACollector
{
protected:
    std::vector<std::shared_ptr<ICollectable>> _items;
    std::vector<bool>                          _removed_signs;
    size_t                                     _removed_count = 0;

    bool    invariant() const
    {
        return _items.size() == _removed_signs.size() && _removed_count <= _items.size();
    }

public:
    virtual ~ACollector() = default;

    virtual std::shared_ptr<ICollectable> read(std::istream& is, bool & flag) = 0;

    size_t getSize() const { return _items.size(); }

    std::shared_ptr<ICollectable> getItem(size_t index) const;

    bool isRemoved(size_t index) const;

    void addItem(std::shared_ptr<ICollectable> item);

    void removeItem(size_t index);

    void updateItem(size_t index, const std::shared_ptr<ICollectable> & item);

    void clean();
    
    void printCollection();

    bool loadCollection(const std::string file_name);

    bool saveCollection(const std::string file_name) const;
};

#endif // HW_L4_INFRASTRUCTURE_LAYER_H
