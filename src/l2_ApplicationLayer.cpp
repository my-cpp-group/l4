#include "hw/l2_ApplicationLayer.h"

using namespace std;

bool Application::performCommand(const vector<string> & args)
{
    if (args.empty())
        return false;

    if (args[0] == "l" || args[0] == "load")
    {
        string filename = (args.size() == 1) ? "hw.data" : args[1];

        if (!_col.loadCollection(filename))
        {
            _out.Output("Ошибка при загрузке файла '" + filename + "'");
            return false;
        }

        return true;
    }

    if (args[0] == "s" || args[0] == "save")
    {
        string filename = (args.size() == 1) ? "hw.data" : args[1];

        if (!_col.saveCollection(filename))
        {
            _out.Output("Ошибка при сохранении файла '" + filename + "'");
            return false;
        }

        return true;
    }

    if (args[0] == "c" || args[0] == "clean")
    {
        if (args.size() != 1)
        {
            _out.Output("Некорректное количество аргументов команды clean");
            return false;
        }

        _col.clean();

        return true;
    }

    if (args[0] == "a" || args[0] == "add")
    {
        if (args.size() != 8)
        {
            _out.Output("Некорректное количество аргументов команды add");
            return false;
        }

        _col.addItem(make_shared<Squad>(stoul(args[1]), stoul(args[2]), stoul(args[3]), stoul(args[4]), stoul(args[5]), stoul(args[6]), stoul(args[7])));
        return true;
    }

    if (args[0] == "r" || args[0] == "remove")
    {
        if (args.size() != 2)
        {
            _out.Output("Некорректное количество аргументов команды remove");
            return false;
        }

        _col.removeItem(stoul(args[1]));
        return true;
    }

    if (args[0] == "u" || args[0] == "update")
    {
        if (args.size() != 9)
        {
            _out.Output("Некорректное количество аргументов команды update");
            return false;
        }

        _col.updateItem(stoul(args[1]), make_shared<Squad>(stoul(args[2]), stoul(args[3]), stoul(args[4]), stoul(args[5]), stoul(args[6]), stoul(args[7]), stoul(args[8])));
        return true;
    }

    if (args[0] == "v" || args[0] == "view")
    {
        if (args.size() != 1)
        {
            _out.Output("Некорректное количество аргументов команды view");
            return false;
        }

       _col.printCollection();
		return true;
    }
    if (args[0] == "f" || args[0] == "fight")
	{
		if (args.size() != 1)
		{
			_out.Output("Некорректное количество аргументов команды fight");
			return false;
		}

		if (_col.getSize()==0)
		{
			_out.Output("В отряде нет бойцов");
			return false;
		}

		Fight fight(_col.getCollection());
		fight.print();
		fight.doFight();
		return true;
	}

    _out.Output("Недопустимая команда '" + args[0] + "'");
    return false;
}
