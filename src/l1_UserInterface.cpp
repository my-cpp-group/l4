#include <iostream>
#include <string>
#include <cassert>

#include <sstream>

#include "hw/l2_ApplicationLayer.h"

using namespace std;

class TerminalOutput : public IOutput
{
public:
    virtual void Output(string s) const override final;
};


void TerminalOutput::Output(string s) const
{
    cout << s << endl;
}


int main(int , char **)
{
    TerminalOutput out;
    Application    app(out);

    string str = "";
		cout << "input a command" << endl;
		getline(cin, str);
		while (str != "")
		{
			int j = 1;
			vector<string> args;
			args.push_back(str);
			while (str != "") {
				cout << "input " << j << "th" << " argument" << endl;
				getline(cin, str);
				j++;
				if (str != "")
				{
					args.push_back(str);
				}
			}
			app.performCommand(args);
			cout << "input a command" << endl;
			getline(cin, str);
		}

    return 0;
}
