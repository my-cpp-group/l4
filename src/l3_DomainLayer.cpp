#include "hw/l3_DomainLayer.h"

using namespace std;


bool Squad::invariant()
{
    bool result= (0 <= this->strength && this->strength <= 100) && (0 <= this->dexterity && this->dexterity <= 100) && (0 <= this->defence && this->defence <= 100) &&(0 <= this->dodging && this->dodging <= 100) && (0 <= this->health && this->health <= 100);
		return result;
}

Squad::Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft) {
		this->fraction = f;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		this->fightType = ft;
		assert(invariant());
	};

Squad::Squad(int f, int strength, int defence, int health, int dexterity, int dodging, int ft) {
		Fraction frac;
		switch (f)
		{
		case 0: frac = Fraction::MAGISTER;
			break;
		case 1: frac = Fraction::PALADIN;
			break;
		case 2: frac = Fraction::GODWOKEN;
			break;
		default:
			frac = Fraction::GODWOKEN;
			break;
		}
		this->fraction = frac;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		FightType Ft;
		switch (ft)
		{
		case 0: Ft = FightType::MELEE;
			break;
		case 1: Ft = FightType::RANGED;
			break;
		default:  Ft = FightType::MELEE;
			break;
		}
		this->fightType = Ft;
		assert(invariant());
	};

void Squad::print() {
		cout << "-------------------------------------------------------------------" << endl;
		switch (this->fraction)
		{
		case Squad::Fraction::GODWOKEN:
			cout << "Fraction: Godwoken" << endl;
			break;
		case Squad::Fraction::MAGISTER:
			cout << "Fraction: Magister" << endl;
			break;
		case Squad::Fraction::PALADIN:
			cout << "Fraction: Paladin" << endl;
			break;
		}

		cout << "Strength: " << this->strength << endl << "Defence: " << this->defence << endl <<
			"Haelth: " << this->health << endl << "Dexterity: " << this->dexterity << endl << "Dodging: " << this->dodging << endl;
		if (this->fightType == Squad::FightType::MELEE) {
			cout << "Fight Type: Melee" << endl;
		}
		else {
			cout << "Fight Type: Ranged" << endl;
		}
		cout << "-------------------------------------------------------------------" << endl;
	};

bool   Squad::write(ostream& os)
{
    Squad temp(this->fraction, this->strength, this->defence, this->health, this->dexterity, this->dodging, this->fightType);
		os.write((char*)&temp, sizeof(Squad));
		return true;

}



shared_ptr<ICollectable> ItemCollector::read(istream& is, bool & flag)
{
    Squad result = readItem<Squad>(is, flag);
		return make_shared<Squad>(result);
}

vector<shared_ptr<ICollectable>> ItemCollector::getCollection() {
		return _items;
}

void Troll::print() {
	cout << "-------------------------------------------------------------------" << endl;
	cout << "Strength: " << this->strength << endl << "Defence: " << this->defence << endl <<
			"Haelth: " << this->health << endl << "Dexterity: " << this->dexterity << endl << "Dodging: " << this->dodging << endl;
	cout << "-------------------------------------------------------------------" << endl;
}

Fight::Fight(vector<std::shared_ptr<ICollectable>> squads){
	int strength = 0;
	int defence = 0;
	int health = 0;
	int dexterity = squads[0]->dexterity;
	int dodging = squads[0]->dodging;
	for (auto item : squads)
	{
		health += item->health;
		if (item->strength>strength)
		{
			strength = item->strength;
		}
		if (item->defence>defence)
		{
			defence = item->defence;
		}
		if (item->dodging<dodging)
		{
			dodging = item->dodging;
		}
		if (item->dexterity<dexterity)
		{
			dexterity = item->dexterity;
		}

		this->_squads.push_back(item);
	}
	_troll = make_shared<Troll>(strength, defence, health, dexterity, dodging);
}

void Fight::squadManagment() {
	for (int i = 0; i < _squads.size();i++) {
		if (_squads[i]->isDead()) {
			_squads.erase(_squads.begin() + i);
		}
	}
}

int Fight::strike(shared_ptr<ICollectable> hitter, shared_ptr<ICollectable> taker) {
	int damage = hitter->strength + hitter->dexterity - taker->dodging - taker->defence;
	if (damage > 0) {
		taker->health = taker->health - damage;
	}
	return damage;
}

void Fight::doFight() {
	while (!this->endOfFight())
	{
		cout << "-------------------ROUND---------------------------" << endl;
		for (auto squad : _squads) {
			int damage = strike(squad, _troll);
			cout << "Squad hit the troll on " << damage << " troll's health is " << _troll->health << endl;
		}
		if (_troll->health>0)
		{
			int damage = strike(_troll, _squads[0]);
			cout << "Troll hits a squad on " << damage << endl;
			cout << "hited squad:" << endl;
			_squads[0]->print();
		}
		this->squadManagment();
	}
	if(_troll->isDead()){
		cout << "Squads have won the fight " << endl;
	}else{
		cout << "Troll has won the fight " << endl;
	}
}



void Fight::print() {
	cout << "Squads:" << endl;
	for (auto item : _squads) {
		item->print();
	}
	cout << "Troll:" << endl;
	_troll->print();
	if (endOfFight()) {
		cout << "End" << endl;
	}
}
